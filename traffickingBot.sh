#! /bin/bash

# This would have been better in python.. oh well.
# cronjob:
# * 8-23 * * * traffickingBot.sh
# */20 * * * * rm already_sent

TIME=$(date)
CITY_NAMES=$(cat Cities.txt | awk '{print $1 }')
MONTH_THRESHOLD=5; # May
TEXT_MESSAGE=()

CallTrafikVerket(){
  locationName=$1
  locationId=$(cat Cities.txt | grep $locationName | awk '{ print $2 }')

  echo "Fetching data from $city"

  curl -s -X POST \
  https://fp.trafikverket.se/Boka/occasion-bundles \
  -H 'Accept: application/json, text/javascript, */*; q=0.01' \
  -H 'Accept-Encoding: gzip, deflate, br' \
  -H 'Accept-Language: en-US,en;q=0.5' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 474' \
  -H 'Content-Type: application/json' \
  -H 'DNT: 1' \
  -H 'Host: fp.trafikverket.se' \
  -H 'Origin: https://fp.trafikverket.se' \
  -H 'Referer: https://fp.trafikverket.se/Boka/' \
  -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0' \
  -H 'X-Requested-With: XMLHttpRequest' \
  -H 'cache-control: no-cache' \
  -d '{"bookingSession":{"socialSecurityNumber":"<SOCIAL_SECURITY_NUMBER>","licenceId":5,"bookingModeId":0,"ignoreDebt":false,"ignoreBookingHindrance":false,"examinationTypeId":0,"excludeExaminationCategories":[],"rescheduleTypeId":0,"paymentIsActive":false,"paymentReference":null,"paymentUrl":null},"occasionBundleQuery":{"startDate":"2021-03-28T22:00:00.000Z","locationId":'$locationId',"nearbyLocationIds":[],"vehicleTypeId":2,"tachographTypeId":1,"occasionChoiceId":1,"examinationTypeId":12}}' > response/"$locationName"_json
}

NotifySubscribers(){
  message="$1"
  echo -e "$TIME: Message to subscribers:\n$message"
  aws sns publish --message "$(echo -e $message)" --topic-arn arn:aws:sns:eu-north-1:<AWS_ACCOUNT_ID>:traffic-topic --subject "KÖRPROV" --profile traffic-user 2> error || echo "$TIME failed to send" >> logs

  curl -s https://api.46elks.com/a1/sms \
    -u <API_USER>:<API_PASSWORD> \
    -d from=PROV \
    -d to=<PHONE_NUMBER> \
    -d message="$(echo -e $message)"
  
  echo -e "$message" >> already_sent;
}

echo "$TIME: Running just so u know" >> logs

 for city in $CITY_NAMES;
 do
   CallTrafikVerket $city
 done;

echo "Processing data..."

for city in $CITY_NAMES;
do
  startDates=$(cat response/${city}_json | jq -r .data[].occasions[0].duration.start | head -n 20) 

  for startDate in $startDates;
  do
    yearMonthDay=$(echo $startDate | awk -F 'T' '{print $1}');
    timeOfDay=$(echo $startDate | awk -F 'T' '{print $2 '} | sed 's/^\([0-9]\{2\}:[0-9]\{2\}\).*/\1/')
    year=$(echo $yearMonthDay | awk -F '-' '{print $1}');
    month=$(echo $yearMonthDay | awk -F '-' '{print $2}' | sed 's/0\([1-9]\{1\}\)/\1/');
  
    if [[ $month -le $MONTH_THRESHOLD && $year -eq 2021 ]];
    then
        TEXT_MESSAGE+="Ledigt körprov i $city $yearMonthDay kl. $timeOfDay\n"
    fi;
    done;
done; 

if [ -z "$TEXT_MESSAGE" ];
then
  echo "Inga lediga körprov";
  for city in $CITY_NAMES;
  do
    statusCode=$(cat response/${city}_json | jq -r .status)
    [ $statusCode -ne 200 ] && echo "$time: Status code to $city was $statusCode" >> error_logs
  done;
  exit 0;
else
  echo -e "$time: Result:\n$TEXT_MESSAGE" >> logs;
  if [ ! -f already_sent ];
  then
    NotifySubscribers "$TEXT_MESSAGE"
  else
    sendMessage=""
    IFS=$'\n'
    for line in $(echo -e "$TEXT_MESSAGE");
    do
      [ -z "$(cat already_sent | grep $line)" ] && sendMessage+="$line"
    done;
    if [ ! -z $sendMessage ];
    then
      NotifySubscribers "$sendMessage"
    else
      echo "Subscribers already notified";
    fi;
  fi;
fi;